package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "update task by index";

    private final static String DESCRIPTION = "update task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getTaskService().updateByIndex(index - 1, name, description));
    }

}
