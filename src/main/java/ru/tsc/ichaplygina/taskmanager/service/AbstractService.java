package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        repository.add(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E findById(final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    public boolean isEmpty() {
        return repository.isEmpty();
    }

    @Override
    public E removeById(final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.removeById(id);
    }

}
