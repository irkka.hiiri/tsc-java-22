package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

public interface IUserService extends IService<User> {

    boolean unlockById(String id);

    boolean unlockByLogin(String login);

    void add(String login, String password, String email, Role role,
             String firstName, String middleName, String lastName);

    User findByLogin(String login);

    boolean lockById(String id);

    boolean lockByLogin(String login);

    User removeByLogin(String login);

    void setPassword(String login, String password);

    void setRole(String login, Role role);

    User updateById(String id, String login, String password, String email,
                    Role role, String firstName, String middleName, String lastName);

    User updateByLogin(String login, String password, String email,
                       Role role, String firstName, String middleName, String lastName);

}
