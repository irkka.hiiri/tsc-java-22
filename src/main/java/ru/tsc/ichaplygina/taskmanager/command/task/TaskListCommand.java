package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListCommand extends AbstractTaskCommand {

    private final static String NAME = "list tasks";

    private final static String DESCRIPTION = "show all tasks";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        if (getTaskService().isEmpty()) {
            printLinesWithEmptyLine("No tasks yet. Type <create task> to add a task.");
            return;
        }
        final Comparator<Task> comparator = readComparator();
        printListWithIndexes(getTaskService().findAll(comparator));
    }

}
