package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    private final static String NAME = "start project by name";

    private final static String DESCRIPTION = "start project by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectService().startByName(name));
    }

}
