package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @Override
    public void add(final String name, final String description, final String userId) {
        final Task task = new Task(name, description, userId);
        map.put(task.getId(), task);
    }

    @Override
    public Task addTaskToProject(final String taskId, final String projectId) {
        Optional<Task> task = Optional.ofNullable(findById(taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Override
    public Task addTaskToProjectForUser(final String userId, final String taskId, final String projectId) {
        Optional<Task> task = Optional.ofNullable(findByIdForUser(userId, taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        return map.values().stream()
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, Comparator<Task> comparator) {
        return map.values().stream()
                .filter(predicateByProjectId(projectId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String userId, final String projectId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String userId, final String projectId, Comparator<Task> comparator) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProjectId(projectId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isNotFoundTaskInProject(final String taskId, final String projectId) {
        return (!findAllByProjectId(projectId).contains(findById(taskId)));
    }

    public Predicate<Task> predicateByProjectId(final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        findAllByProjectId(projectId).forEach(task -> map.remove(task.getId()));
    }

    @Override
    public Task removeTaskFromProject(final String taskId, final String projectId) {
        final Optional<Task> task = Optional.ofNullable(findById(taskId)).filter(predicateByProjectId(projectId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

    @Override
    public Task removeTaskFromProjectForUser(final String userId, final String taskId, final String projectId) {
        final Optional<Task> task = Optional.ofNullable(findByIdForUser(userId, taskId)).filter(predicateByProjectId(projectId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

}
