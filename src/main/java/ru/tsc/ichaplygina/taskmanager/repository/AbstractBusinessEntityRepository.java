package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractModelRepository<E> implements IBusinessEntityRepository<E> {

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void clearForUser(final String userId) {
        findKeysForUser(userId).forEach(map::remove);
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return map.values().stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public List<E> findAllForUser(final String userId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAllForUser(final String userId, Comparator<E> comparator) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findByIdForUser(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        return entity.filter(predicateByUser(userId)).orElse(null);
    }

    @Override
    public E findByIndex(final int index) {
        return map.values().stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndexForUser(final String userId, final int index) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByName(final String name) {
        return map.values().stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByNameForUser(final String userId, final String name) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<String> findKeysForUser(final String userId) {
        return map.values().stream()
                .filter(predicateByUser(userId))
                .map(E::getId)
                .collect(Collectors.toList());
    }

    @Override
    public String getId(final String name) {
        return Optional.ofNullable(findByName(name)).orElse(null).getId();
    }

    @Override
    public String getId(final int index) {
        return Optional.ofNullable(findByIndex(index)).orElse(null).getId();
    }

    @Override
    public String getIdForUser(final String userId, final int index) {
        return Optional.ofNullable(findByIndexForUser(userId, index)).orElse(null).getId();
    }

    @Override
    public String getIdForUser(final String userId, final String name) {
        return Optional.ofNullable(findByNameForUser(userId, name)).orElse(null).getId();
    }

    @Override
    public int getSizeForUser(final String userId) {
        return findAllForUser(userId).size();
    }

    @Override
    public boolean isEmptyForUser(final String userId) {
        return findAllForUser(userId).isEmpty();
    }

    @Override
    public boolean isNotFoundById(final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(final String userId, final String id) {
        return findByIdForUser(userId, id) == null;
    }

    public Predicate<AbstractBusinessEntity> predicateByName(final String name) {
        return s -> name.equals(s.getName());
    }

    public Predicate<AbstractBusinessEntity> predicateByUser(final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Override
    public E removeByIdForUser(final String userId, final String id) {
        final E entity = findByIdForUser(userId, id);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E removeByIndex(final int index) {
        final E entity = findByIndex(index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E removeByIndexForUser(final String userId, final int index) {
        final E entity = findByIndexForUser(userId, index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E removeByName(final String name) {
        final E entity = findByName(name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E removeByNameForUser(final String userId, final String name) {
        final E entity = findByNameForUser(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public E update(final String id, final String name, final String description) {
        final E entity = findById(id);
        return updateEntity(entity, name, description).orElse(null);
    }

    private Optional<E> updateEntity(final E entity, final String name, final String description) {
        final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return optional;
    }

    @Override
    public E updateForUser(final String userId, final String id, final String name, final String description) {
        final E entity = (findByIdForUser(userId, id));
        return updateEntity(entity, name, description).orElse(null);
    }

    private Optional<E> updateStatus(final E entity, final Status status) {
        final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setStatus(status);
            if (status.equals(Status.IN_PROGRESS)) e.setDateStart(new Date());
            else if (status.equals(Status.COMPLETED)) e.setDateFinish(new Date());
        });
        return optional;
    }

    @Override
    public E updateStatusById(final String id, final Status status) {
        final E entity = findById(id);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E updateStatusByIdForUser(final String userId, final String id, final Status status) {
        final E entity = findByIdForUser(userId, id);
        return updateStatus(entity, status).orElse(null);
    }

    @Override
    public E updateStatusByIndex(final int index, final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getId(index));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Override
    public E updateStatusByIndexForUser(final String userId, final int index, final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, index));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

    @Override
    public E updateStatusByName(final String name, final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getId(name));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Override
    public E updateStatusByNameForUser(final String userId, final String name, final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdForUser(userId, name));
        return entityId.map(id -> updateStatusByIdForUser(userId, id, status)).orElse(null);
    }

}
