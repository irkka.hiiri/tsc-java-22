package ru.tsc.ichaplygina.taskmanager.util;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class HashUtil {

    private static final int ITERATION = 66666;
    private static final String SECRET = "2u34hr3wueu3wfd";

    public static String md5(final String value) {
        if (isEmptyString(value)) return null;
        try {
            final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(value.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String salt(final String value) {
        if (isEmptyString(value)) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) result = md5(SECRET + result + SECRET);
        return result;
    }

}
