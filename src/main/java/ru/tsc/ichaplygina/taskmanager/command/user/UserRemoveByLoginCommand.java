package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.security.UserSelfDeleteNotAllowedException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    private static final String NAME = "remove user by login";

    private static final String DESCRIPTION = "remove user by login";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        final String login = readLine(ENTER_LOGIN);
        if (login.equals(getAuthService().getCurrentUserLogin())) throw new UserSelfDeleteNotAllowedException();
        throwExceptionIfNull(getUserService().removeByLogin(login));
    }

}
