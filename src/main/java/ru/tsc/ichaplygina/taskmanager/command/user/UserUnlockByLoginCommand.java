package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    private static final String NAME = "unlock user by login";

    private static final String DESCRIPTION = "unlock user by login";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        final String login = readLine(ENTER_LOGIN);
        if (!getUserService().unlockByLogin(login)) printLinesWithEmptyLine(USER_IS_NOT_LOCKED);
    }

}
