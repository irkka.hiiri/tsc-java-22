package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "start task by id";

    private final static String DESCRIPTION = "start task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().startById(id));
    }

}
