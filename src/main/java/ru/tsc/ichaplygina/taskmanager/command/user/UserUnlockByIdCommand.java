package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserUnlockByIdCommand extends AbstractUserCommand {

    private static final String NAME = "unlock user by id";

    private static final String DESCRIPTION = "unlock user by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        if (!getUserService().unlockById(id)) printLinesWithEmptyLine(USER_IS_NOT_LOCKED);
    }

}
