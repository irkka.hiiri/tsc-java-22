package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "change password";

    private static final String DESCRIPTION = "change user's password";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String currentUserId = getAuthService().getCurrentUserId();
        final String login = getAuthService().isPrivilegedUser() ? readLine(ENTER_LOGIN) : getUserService().findById(currentUserId).getLogin();
        final String password = readLine(ENTER_PASSWORD);
        getUserService().setPassword(login, password);
    }

}
