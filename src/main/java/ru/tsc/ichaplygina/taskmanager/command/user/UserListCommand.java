package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printList;

public class UserListCommand extends AbstractUserCommand {

    private static final String NAME = "list users";

    private static final String DESCRIPTION = "show all users";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        if (getUserService().isEmpty()) {
            printLinesWithEmptyLine("No users found!");
            return;
        }
        printList(getUserService().findAll());
    }

}
