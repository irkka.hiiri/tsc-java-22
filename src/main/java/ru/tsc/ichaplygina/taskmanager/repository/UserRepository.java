package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractModelRepository<User> implements IUserRepository {

    public Predicate<User> predicateByEmail(final String email) {
        return s -> email.equals(s.getEmail());
    }

    public Predicate<User> predicateByLogin(final String login) {
        return s -> login.equals(s.getLogin());
    }

    @Override
    public User findByEmail(final String email) {
        return map.values().stream()
                .filter(predicateByEmail(email))
                .findFirst().orElse(null);
    }

    @Override
    public User findById(final String id) {
        return map.get(id);
    }

    @Override
    public User findByLogin(final String login) {
        return map.values().stream()
                .filter(predicateByLogin(login))
                .findFirst().orElse(null);
    }

    @Override
    public String findIdByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        return user.map(User::getId).orElse(null);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean isFoundByEmail(final String email) {
        return (findByEmail(email)) != null;
    }

    @Override
    public boolean isFoundByLogin(final String login) {
        return (findByLogin(login)) != null;
    }

    @Override
    public void remove(final User user) {
        map.remove(user.getId());
    }

    @Override
    public void removeByLogin(final String login) {
        Optional.ofNullable(findByLogin(login)).ifPresent(this::remove);
    }

    @Override
    public User update(final String id, final String login, final String password, final String email,
                       final Role role, final String firstName, final String middleName, final String lastName) {
        final Optional<User> user = Optional.ofNullable(findById(id));
        user.ifPresent(u -> {
            u.setLogin(login);
            u.setPasswordHash(password);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
            u.setLastName(lastName);
        });
        return user.orElse(null);
    }

}
