package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListByProjectCommand extends AbstractTaskCommand {

    private final static String NAME = "list tasks by project";

    private final static String DESCRIPTION = "show all tasks in a project";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final Comparator<Task> taskComparator = readComparator();
        final List<Task> taskList = getProjectTaskService().findAllTasksByProjectId(projectId, taskComparator);
        if (taskList.isEmpty()) {
            printLinesWithEmptyLine("No tasks yet. Type <add task to project> to add a task to a project.");
            return;
        }
        printListWithIndexes(taskList);
    }

}
