package ru.tsc.ichaplygina.taskmanager.command;

import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractCommand {

    public static final String ID_INPUT = "Please enter id: ";

    public static final String INDEX_INPUT = "Please enter index: ";

    public static final String NAME_INPUT = "Please enter name: ";

    public static final String DESCRIPTION_INPUT = "Please enter description: ";

    protected ServiceLocator serviceLocator;

    protected Boolean needAuthorization = false;

    public abstract void execute();

    public abstract String getArgument();

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public abstract String getCommand();

    public abstract String getDescription();

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

    public Role[] getRoles() {
        return Role.values();
    }

    public Boolean getNeedAuthorization() {
        return needAuthorization;
    }

    public void setNeedAuthorization(Boolean needAuthorization) {
        this.needAuthorization = needAuthorization;
    }
}
