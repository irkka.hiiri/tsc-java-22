package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private final static String NAME = "remove project by id";

    private final static String DESCRIPTION = "remove project by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectById(id));
    }

}
