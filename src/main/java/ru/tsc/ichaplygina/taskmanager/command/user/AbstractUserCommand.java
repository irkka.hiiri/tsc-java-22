package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected final String ENTER_LOGIN = "Enter login: ";

    protected final String ENTER_PASSWORD = "Enter password: ";

    protected final String ENTER_EMAIL = "Enter email: ";

    protected final String ENTER_ROLE = "Enter user role: (default: User) ";

    protected final String ENTER_FIRST_NAME = "Enter first name: ";

    protected final String ENTER_MIDDLE_NAME = "Enter middle name: ";

    protected final String ENTER_LAST_NAME = "Enter last name: ";

    protected final String USER_ALREADY_LOCKED = "User already locked. ";

    protected final String USER_IS_NOT_LOCKED = "User is not locked. ";

    {
        setNeedAuthorization(true);
    }

    public String getArgument() {
        return null;
    }

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected void showUser(final User user) {
        printLinesWithEmptyLine(Optional.ofNullable(user).orElseThrow(UserNotFoundException::new));
    }

    protected void throwExceptionIfNull(final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
    }

}
