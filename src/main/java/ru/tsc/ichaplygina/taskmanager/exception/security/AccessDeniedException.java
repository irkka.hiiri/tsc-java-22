package ru.tsc.ichaplygina.taskmanager.exception.security;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    private static final String MESSAGE = "Access denied!";

    public AccessDeniedException() {
        super(MESSAGE);
    }

}
