package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByEmail(String email);

    User findByLogin(String login);

    String findIdByLogin(String login);

    boolean isFoundByEmail(String email);

    boolean isFoundByLogin(String login);

    void removeByLogin(String login);

    User update(String id, String login, String password, String email,
                Role role, String firstName, String middleName, String lastName);
}
