package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class ListCommandsCommand extends AbstractCommand {

    private final static String CMD_NAME = "list arguments";

    private final static String DESCRIPTION = "list available command-line arguments";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println();
        for (AbstractCommand command : serviceLocator.getCommandService().getCommandList()) {
            if (isEmptyString(command.getArgument())) continue;
            System.out.println(command.getArgument());
        }
        System.out.println();
    }

}
