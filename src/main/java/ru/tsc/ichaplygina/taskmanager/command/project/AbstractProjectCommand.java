package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractProjectCommand extends AbstractCommand {

    {
        setNeedAuthorization(true);
    }

    public String getArgument() {
        return null;
    }

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(final Project project) {
        printLinesWithEmptyLine(Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new));
    }

    protected void throwExceptionIfNull(final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
