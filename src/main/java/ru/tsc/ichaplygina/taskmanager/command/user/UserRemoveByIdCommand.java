package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.security.UserSelfDeleteNotAllowedException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    private static final String NAME = "remove user by id";

    private static final String DESCRIPTION = "remove user by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        if (id.equals(getAuthService().getCurrentUserId())) throw new UserSelfDeleteNotAllowedException();
        throwExceptionIfNull(getUserService().removeById(id));
    }

}
