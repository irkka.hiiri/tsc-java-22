package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readRole;

public class UserUpdateByIdCommand extends AbstractUserCommand {

    private static final String NAME = "update user by id";

    private static final String DESCRIPTION = "update user by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        final String login = readLine(ENTER_LOGIN);
        final String password = readLine(ENTER_PASSWORD);
        final String email = readLine(ENTER_EMAIL);
        final Role role = readRole(ENTER_ROLE);
        final String firstName = readLine(ENTER_FIRST_NAME);
        final String middleName = readLine(ENTER_MIDDLE_NAME);
        final String lastName = readLine(ENTER_LAST_NAME);
        throwExceptionIfNull(getUserService().updateById(id, login, password, email, role, firstName, middleName, lastName));
    }

}
