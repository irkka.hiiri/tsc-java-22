package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserExistsWithLoginException extends AbstractException {

    private static final String MESSAGE = "User already exists with login: ";

    public UserExistsWithLoginException(final String login) {
        super(MESSAGE + login);
    }

}
