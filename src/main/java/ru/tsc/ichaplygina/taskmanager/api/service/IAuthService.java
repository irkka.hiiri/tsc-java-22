package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

    String getCurrentUserLogin();

    void checkRoles(Role[] roles);

    boolean isNoUserLoggedIn();

    boolean isPrivilegedUser();

    void login(String login, String password);

    void logout();

    void throwExceptionIfNotAuthorized();

}
