package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class User extends AbstractModel {

    private String email;

    private String firstName;

    private String lastName;

    private String login;

    private String middleName;

    private String passwordHash;

    private Role role;

    private boolean locked;

    public User(final String login, final String passwordHash, final String email, final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.role = role;
        this.locked = false;
    }

    public User(final String login, final String passwordHash, final String email, final String firstName,
                final String middleName, final String lastName, final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.role = role;
        this.locked = false;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getFullName() {
        return (isEmptyString(firstName) ? "" : firstName + " ") +
                (isEmptyString(middleName) ? "" : middleName + " ") +
                (isEmptyString(lastName) ? "" : lastName + " ");
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return getId() + DELIMITER + login + DELIMITER + email + DELIMITER + "Role: " + role +
                (isEmptyString(getFullName()) ? "" : DELIMITER + "Full Name: " + getFullName());
    }

}
