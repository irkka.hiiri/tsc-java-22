package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectCreateCommand extends AbstractProjectCommand {

    private final static String NAME = "create project";

    private final static String DESCRIPTION = "create a new project";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().add(name, description);
        printLinesWithEmptyLine("Done. Type <list projects> to view all projects.");
    }

}
