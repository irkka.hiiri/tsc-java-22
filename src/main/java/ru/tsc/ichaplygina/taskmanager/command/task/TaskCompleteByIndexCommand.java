package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "complete task by index";

    private final static String DESCRIPTION = "complete task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getTaskService().completeByIndex(index - 1));
    }

}
