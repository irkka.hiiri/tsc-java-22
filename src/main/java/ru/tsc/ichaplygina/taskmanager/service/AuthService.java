package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IAuthRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotLoggedInException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class AuthService implements IAuthService {

    private final IAuthRepository authRepository;

    private final IUserService userService;

    public AuthService(final IAuthRepository authRepository, final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        return authRepository.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        authRepository.setCurrentUserId(currentUserId);
    }

    @Override
    public String getCurrentUserLogin() {
        throwExceptionIfNotAuthorized();
        return userService.findById(getCurrentUserId()).getLogin();
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        if (isNoUserLoggedIn()) return;
        final User user = userService.findById(getCurrentUserId());
        for (Role role : roles) {
            if (user.getRole().equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public boolean isNoUserLoggedIn() {
        return isEmptyString(authRepository.getCurrentUserId());
    }

    @Override
    public boolean isPrivilegedUser() {
        throwExceptionIfNotAuthorized();
        return userService.findById(getCurrentUserId()).getRole().equals(Role.ADMIN);
    }

    @Override
    public void login(final String login, final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(userService.findByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
        if (!user.getPasswordHash().equals(salt(password))) throw new IncorrectCredentialsException();
        if (user.isLocked()) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (isNoUserLoggedIn()) throw new UserNotLoggedInException();
        setCurrentUserId(null);
    }

    @Override
    public void throwExceptionIfNotAuthorized() {
        if (isNoUserLoggedIn()) throw new AccessDeniedNotAuthorizedException();
    }

}
