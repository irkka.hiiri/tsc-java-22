package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectListCommand extends AbstractProjectCommand {

    private final static String NAME = "list projects";

    private final static String DESCRIPTION = "show all projects";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        if (getProjectService().isEmpty()) {
            printLinesWithEmptyLine("No projects yet. Type <create project> to add a project.");
            return;
        }
        final Comparator<Project> comparator = readComparator();
        printListWithIndexes(getProjectService().findAll(comparator));
    }

}
