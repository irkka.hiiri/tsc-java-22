package ru.tsc.ichaplygina.taskmanager.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(String message) {
        super(message);
    }

}
