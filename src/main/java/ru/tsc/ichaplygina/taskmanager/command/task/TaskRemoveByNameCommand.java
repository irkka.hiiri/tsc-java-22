package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    private final static String NAME = "remove task by name";

    private final static String DESCRIPTION = "remove task by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getTaskService().removeByName(name));
    }

}
