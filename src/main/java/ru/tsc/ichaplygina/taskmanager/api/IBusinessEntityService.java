package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityService<E extends AbstractBusinessEntity> extends IService<E> {

    void add(String name, String description);

    void clear();

    E completeById(String id);

    E completeByIndex(int index);

    E completeByName(String name);

    List<E> findAll(Comparator<E> comparator);

    E findByIndex(int index);

    E findByName(String name);

    String getId(int index);

    String getId(String name);

    boolean isNotFoundById(String id);

    E removeByIndex(int index);

    E removeByName(String name);

    E startById(String id);

    E startByIndex(int index);

    E startByName(String name);

    E updateById(String id, String name, String description);

    E updateByIndex(int index, String name, String description);

    E updateStatusById(String id, Status status);

    E updateStatusByIndex(int index, Status status);

    E updateStatusByName(String name, Status status);

}
