package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public class ProjectClearCommand extends AbstractProjectCommand {

    private final static String NAME = "clear projects";

    private final static String DESCRIPTION = "delete all projects";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int count = getProjectService().getSize();
        getProjectTaskService().clearProjects();
        printLinesWithEmptyLine(count + " projects removed");
    }

}
