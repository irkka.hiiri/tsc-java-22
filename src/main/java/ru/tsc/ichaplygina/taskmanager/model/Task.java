package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    public Task() {
    }

    public Task(final String name, final String userId) {
        super(name, userId);
    }

    public Task(final String name, final String description, final String userId) {
        super(name, description, userId);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}
