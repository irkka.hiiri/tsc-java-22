package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "complete project by index";

    private final static String DESCRIPTION = "complete project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectService().completeByIndex(index - 1));
    }

}
