package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "complete task by id";

    private final static String DESCRIPTION = "complete task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().completeById(id));
    }

}
