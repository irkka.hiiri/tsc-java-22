package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    private final static String NAME = "complete project by name";

    private final static String DESCRIPTION = "complete project by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectService().completeByName(name));
    }

}
