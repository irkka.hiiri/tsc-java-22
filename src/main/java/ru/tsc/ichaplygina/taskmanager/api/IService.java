package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    void add(E entity);

    List<E> findAll();

    E findById(String id);

    int getSize();

    boolean isEmpty();

    E removeById(String id);
}
