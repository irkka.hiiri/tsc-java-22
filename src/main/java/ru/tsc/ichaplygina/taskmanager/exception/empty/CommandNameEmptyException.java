package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class CommandNameEmptyException extends AbstractException {

    private static final String MESSAGE = "Error! Command name cannot be empty.";

    public CommandNameEmptyException() {
        super(MESSAGE);
    }
}
