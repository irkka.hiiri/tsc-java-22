package ru.tsc.ichaplygina.taskmanager.api.entity;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
