package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class EmailEmptyException extends AbstractException {

    private static final String MESSAGE = "Error! Email is empty.";

    public EmailEmptyException() {
        super(MESSAGE);
    }

}
