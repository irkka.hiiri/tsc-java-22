package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;

public abstract class AbstractBusinessEntity extends AbstractModel implements IWBS {

    private Date created = new Date();

    private Date dateFinish;

    private Date dateStart;

    private String description;

    private String name;

    private Status status = Status.PLANNED;

    private String userId;

    public AbstractBusinessEntity() {
        this.name = EMPTY;
        this.description = EMPTY;
    }

    public AbstractBusinessEntity(final String name, final String userId) {
        this.name = name != null ? name : EMPTY;
        this.userId = userId;
    }

    public AbstractBusinessEntity(final String name, final String description, final String userId) {
        this.name = name != null ? name : EMPTY;
        this.description = description != null ? description : EMPTY;
        this.userId = userId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return getId()
                + DELIMITER
                + (!name.equals(EMPTY) ? name : PLACEHOLDER)
                + DELIMITER
                + (!description.equals(EMPTY) ? description : PLACEHOLDER)
                + DELIMITER
                + (created == null ? PLACEHOLDER : formatter.format(created))
                + DELIMITER
                + status
                + DELIMITER
                + (dateStart == null ? PLACEHOLDER : formatter.format(dateStart))
                + DELIMITER
                + (dateFinish == null ? PLACEHOLDER : formatter.format(dateFinish))
                + DELIMITER
                + getUserId();
    }

}
