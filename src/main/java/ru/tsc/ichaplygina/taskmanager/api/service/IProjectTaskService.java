package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    Task addTaskToProject(String projectId, String taskId);

    void clearProjects();

    List<Task> findAllTasksByProjectId(String projectId, Comparator<Task> taskComparator)
            ;

    Project removeProjectById(String projectId);

    Project removeProjectByIndex(int projectIndex);

    Project removeProjectByName(String name);

    Task removeTaskFromProject(String projectId, String taskId);

}
