package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    private final static String NAME = "show task by name";

    private final static String DESCRIPTION = "show task by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String name = readLine(NAME_INPUT);
        final Task task = getTaskService().findByName(name);
        showTask(task);
    }

}
