package ru.tsc.ichaplygina.taskmanager.exception.security;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserSelfLockNotAllowedException extends AbstractException {

    private static final String MESSAGE = "Cannot lock logged-on user.";

    public UserSelfLockNotAllowedException() {
        super(MESSAGE);
    }

}
