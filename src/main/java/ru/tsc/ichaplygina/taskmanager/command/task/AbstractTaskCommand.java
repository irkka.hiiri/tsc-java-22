package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public static final String TASK_ID_INPUT = "Please enter task id: ";

    public static final String PROJECT_ID_INPUT = "Please enter project id: ";

    {
        setNeedAuthorization(true);
    }

    public String getArgument() {
        return null;
    }

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showTask(final Task task) {
        printLinesWithEmptyLine(Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new));
    }

    protected void throwExceptionIfNull(final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
