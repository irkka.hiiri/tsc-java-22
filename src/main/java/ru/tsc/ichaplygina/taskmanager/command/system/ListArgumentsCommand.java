package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

public class ListArgumentsCommand extends AbstractCommand {

    private final static String CMD_NAME = "list commands";

    private final static String DESCRIPTION = "list available commands";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println();
        for (AbstractCommand command : serviceLocator.getCommandService().getCommandList()) {
            System.out.println(command.getCommand());
        }
        System.out.println();
    }

}
