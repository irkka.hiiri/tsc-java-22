package ru.tsc.ichaplygina.taskmanager.command.user;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public class WhoAmICommand extends AbstractUserCommand {

    private static final String NAME = "whoami";

    private static final String DESCRIPTION = "print current user login";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        printLinesWithEmptyLine(getUserService().findById(getAuthService().getCurrentUserId()));
    }

}
